var React = require('react');
var ReactDOM = require('react-dom');
var socket = require('socket.io-client')('http://localhost:8888');

var Clock = React.createClass({
  getInitialState: function () {
    return { time: new Date(),
              on: "on" }
  },

  componentDidMount: function () {
    this.intervalId = setInterval(this.tick, 1000);
    // socket.on('connect', this.emitMessage);
  },

  emitMessage: function () {
    var events = ["Meteorite hits!", "Water drought!", "Plants grow!", "Joy fills the room!"];
    var eventNumber = Math.floor(Math.random() * events.length);
    socket.emit('event', events[eventNumber] + " " + this.state.time + " Pod" + this.props.podNumber + " " + this.state.on);
  },

  tick: function () {
    this.setState({
      time: new Date()
    });
  },

  toggleState: function () {
    var click = (this.state.on === "on") ? "off" : "on";
    this.setState({
      on: click
    });
  },

  render: function () {
    return (
      <div className='clock'>
        <p onClick={this.emitMessage}>{ this.state.time.toTimeString().slice(0, 8) }</p>
        <h5 onClick={this.toggleState}>{this.state.on}</h5>
      </div>
    );
  }
});


document.addEventListener("DOMContentLoaded", function () {
  ReactDOM.render(<Clock podNumber={1}/>, document.getElementById('pod1'));
  ReactDOM.render(<Clock podNumber={2}/>, document.getElementById('pod2'));
  ReactDOM.render(<Clock podNumber={3}/>, document.getElementById('pod3'));
  ReactDOM.render(<Clock podNumber={4}/>, document.getElementById('pod4'));
});
