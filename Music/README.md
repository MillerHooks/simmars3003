# Procedural Music and Seeding Stubs ##

## Pure Data (https://puredata.info/downloads/pd-extended) ##
- https://puredata.info/downloads/pd-extended

### Pure Data MTL | Tools for Working with Pure Data Easy ###
- https://github.com/patricksebastien/mtl

### Procedural Dubstep | Burning Man Licensed ###
- https://github.com/dropship/special-delivery

## Csound / CsoundAC ##
- http://iainmccurdy.org/csound.html
- http://www.linuxjournal.com/content/introducing-csoundac-algorithmic-composition-csound-and-python

