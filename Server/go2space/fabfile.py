from fabric.api import env,local
import time
import itertools

def generate_events():
    local("docker-compose -f dev.yml run django python manage.py genereate_events")

def dump_fixtures():
    """
    Dump database fixtures for the environment. [WARNING] This can cause conflicts. Please use caution on merging.

    """
    local('docker-compose run django python manage.py dumpdata geotags > /app/fixtures/geotags.json')

def load_fixtures():
    """
    Loads fixtures into the database for environment configs. This can help if you're getting database errors.
    """
    local('docker-compose run django python manage.py loaddata /app/fixtures/geotags.json')

def migrate_db():
    """
    This will migrate any changes to the database models that may have occured and load any fixtures required.
    """
    local("docker-compose run django python manage.py migrate")

def create_user():
    """
    Create a superuser for the environment admin

    """
    local("docker-compose run django python manage.py createsuperuser")


def start(adduser=False):
    """
    A good place to start if you've already done setup. Runs Shipyard, migrates db changes, loads fixtures, launches browser window with the portal
    """
    from random import randint
    local('docker-compose -f dev.yml build')

    print("Thinking....")
    time.sleep(3)
    print("still thinking.")
    print("Almost there.....")
    for _ in itertools.repeat(None, 3):
        time.sleep(1)
        print("." * randint(3,20))
        time.sleep(3)
        print("..." * randint(3,20))

    local('docker-compose -f dev.yml run django python manage.py migrate')
    local('docker-compose -f dev.yml run django python manage.py createsuperuser')
    local('docker-compose -f dev.yml up')


def stop_all():
    """
    Stop all running docker containers.
    """

    local('docker stop $(docker ps -a -q)')

def host_ip():
    """
    Your docker-machine host IP

    """
    local("docker-machine ip default")