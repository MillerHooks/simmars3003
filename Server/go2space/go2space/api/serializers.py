from rest_framework import serializers
from go2space.geotags.models import StationPoint, Day, Event
from rest_framework import serializers
from rest_framework import generics


class StationPointSerializer(serializers.ModelSerializer):
    class Meta:
        model=StationPoint

class DaySerializer(serializers.ModelSerializer):
    class Meta:
        model=Day

class EventSerializer(serializers.ModelSerializer):
    day = DaySerializer(read_only=True)
    class Meta:
        model=Event
