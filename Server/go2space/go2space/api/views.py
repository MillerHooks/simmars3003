from django.shortcuts import render
from rest_framework import viewsets
from go2space.api.serializers import StationPointSerializer, DaySerializer,  EventSerializer
from go2space.geotags.models import StationPoint, Day, Event

from rest_framework_gis.filters import DistanceToPointFilter


class StationPointViewSet(viewsets.ModelViewSet):
    """
    This viewset automatically provides `list` and `detail` actions.
    """

    queryset = StationPoint.objects.all()
    serializer_class = StationPointSerializer

class DayViewSet(viewsets.ModelViewSet):
    """
    This viewset automatically provides `list` and `detail` actions.
    """
    queryset = Day.objects.all()
    serializer_class = DaySerializer

class EventViewSet(viewsets.ModelViewSet):
    """
    This viewset automatically provides `list` and `detail` actions.
    """

    queryset = Event.objects.all()
    distance_filter_field = 'point'
    filter_backends = (DistanceToPointFilter, )
    serializer_class =  EventSerializer
