from django.conf.urls import url, include
from rest_framework.urlpatterns import format_suffix_patterns
from .views import StationPointViewSet, DayViewSet, EventViewSet
from rest_framework.routers import DefaultRouter


router = DefaultRouter()
router.register(r'station_point', StationPointViewSet)
router.register(r'day', DayViewSet)
router.register(r'events', EventViewSet)


urlpatterns = [
    url(r'^', include(router.urls)),

]

