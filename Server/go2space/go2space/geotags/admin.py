from django.contrib import admin

from go2space.geotags import models


__custom_admins__ = {

}

for model in models.__admin__:
    params = [getattr(models, model)]
    if model in __custom_admins__.keys():
        params.append(__custom_admins__[model])
    else:
        _dyn_class = type('%sAdmin' % ( model,),
            (admin.ModelAdmin,), {} )
        #( VersionAdmin, ), {} )
        params.append(_dyn_class)
    admin.site.register(*params)