from django.contrib.gis.db import models

from django.utils.encoding import python_2_unicode_compatible
from django.contrib.gis.geos import Point



__admin__ = (
    'StationPoint', 'Day', 'Event'
)

EVENTS = {
    'a' : ['PLANT', 'ALGAE'],
    'e' : ['SOLAR', 'NUCLEAR'],
    'f' : ['HARVEST', 'LIVESTOCK'],
    'p' : ['BABY', 'ADULT'],
    'h' : ['PARTY', 'CLUBHOUSE']
}

WORLD_METRIC_CHOICES = (
        ('a', 'Atmosphere'),
        ('e', 'Energy'),
        ('f', 'Food'),
        ('p', 'Population'),
        ('h', 'Happiness')
    )

@python_2_unicode_compatible
class StationPoint(models.Model):
    """
    Geo Event

    """


    name = models.CharField(max_length=255)
    slug = models.SlugField(blank=True)
    trigger_time = models.DateTimeField(blank=True, null=True)
    world_metric = models.CharField(max_length=1, choices=WORLD_METRIC_CHOICES)
    chance_to_hit = models.FloatField(blank=True, null=True)
    gain = models.NullBooleanField()
    health = models.FloatField(blank=True, null=True, default=1.0)
    lon = models.FloatField(blank=True, null=True)
    lat = models.FloatField(blank=True, null=True)
    point = models.PointField(blank=True, null=True)

    def __str__(self):
        return self.name

    def save(self, *args, **kwargs):
        self.point = Point(selv.lat, self.lon)
        super(Event, self).save(*args, **kwargs)

@python_2_unicode_compatible
class Day(models.Model):
    """
    Day

    """
    name = models.CharField(max_length=255, blank=True)
    day = models.DateField(blank=True, null=True)
    difficulty = models.FloatField(blank=True, null=True)


@python_2_unicode_compatible
class Event(models.Model):
    """
    Geo Event

    """
    WORLD_METRIC_CHOICES = (
        ('a', 'Atmosphere'),
        ('e', 'Energy'),
        ('f', 'Food'),
        ('p', 'Population'),
        ('h', 'Happiness')
    )

    name = models.CharField(max_length=255)
    day = models.ForeignKey(Day)
    slug = models.SlugField(blank=True)
    trigger_time = models.DateTimeField(blank=True, null=True)
    world_metric = models.CharField(max_length=1, choices=WORLD_METRIC_CHOICES)
    chance_to_hit = models.FloatField(blank=True, null=True)
    gain = models.NullBooleanField()
    lon = models.FloatField(blank=True, null=True)
    lat = models.FloatField(blank=True, null=True)
    point = models.PointField(blank=True, null=True)

    def __str__(self):
        return self.name

    def save(self, *args, **kwargs):
        self.point = Point(self.lat, self.lon)
        super(Event, self).save(*args, **kwargs)