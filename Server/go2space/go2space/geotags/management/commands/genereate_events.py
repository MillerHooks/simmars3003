from go2space.geotags.models import Event, Day, StationPoint, WORLD_METRIC_CHOICES, EVENTS

from django.core.management import BaseCommand

from random import uniform, choice
import datetime
import time

from dateutil.rrule import rrule, DAILY, MINUTELY



#The class must be named Command, and subclass BaseCommand
class Command(BaseCommand):
    """
     WORLD_METRIC_CHOICES = (
        ('a', 'Atmosphere'),
        ('e', 'Energy'),
        ('f', 'Food'),
        ('p', 'Population'),
        ('h', 'Happiness')
    )

    name = models.CharField(max_length=255)
    day = models.ForeignKey(Day)
    slug = models.SlugField(blank=True)
    trigger_time = models.DateTimeField(blank=True, null=True)
    world_metric = models.CharField(max_length=1, choices=WORLD_METRIC_CHOICES)
    chance_to_hit = models.FloatField(blank=True, null=True)
    gain = models.NullBooleanField()
    lon = models.FloatField(blank=True, null=True)
    lat = models.FloatField(blank=True, null=True)
    point = models.PointField(blank=True, null=True)

    """

    # Show this when the user types help
    help = "My test command"



    # A command must define handle()
    def handle(self, *args, **options):
        days = range(1, 3)
        for day in days:
            day_obj = Day()
            day_next = datetime.date.today() + datetime.timedelta(day+1)
            day_location =  datetime.date.today() + datetime.timedelta(day)
            tm_stme = datetime.time(0, 0, 0)

            day_obj.day = datetime.datetime.combine(day_location, tm_stme)
            day_obj.difficulty = uniform(0, 1)
            #import pdb; pdb.set_trace()
            day_obj.save()
            events_list = list(
                     rrule(MINUTELY,
                           dtstart=day_obj.day.date(),
                           until=datetime.datetime.combine(day_next, tm_stme)
                     )
                )

            for event_tick in events_list:
                event = Event()
                event.day = day_obj
                event.trigger_time = event_tick

                event.gain =choice([True, False])
                event.world_metric = choice(WORLD_METRIC_CHOICES)[0]
                event.name = choice(EVENTS[event.world_metric])
                event.chance_to_hit = uniform(0,1)


                event.lat = uniform(-180,180)
                event.lon = uniform(-90, 90)
                event.save()
                print("name: ", event.name, " |  point: " +  str(event.point) + " | trigger time: " + str(event.trigger_time))
